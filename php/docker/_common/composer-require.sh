#!/bin/bash

PACKAGE=$1
VERSION=${2-*}
REPOSITORY=$3
FOLDER=${PACKAGE//\//-}

mkdir -p /opt/${FOLDER}

COMPOSER="{"
[[ "${REPOSITORY}" != "" ]] && COMPOSER="${COMPOSER}\"repositories\": [{\"type\": \"vcs\",\"url\": \"${REPOSITORY}\"}],"
COMPOSER="${COMPOSER}\"require\": {\"${PACKAGE}\": \"${VERSION}\"}"
COMPOSER="${COMPOSER}}"
echo $COMPOSER
echo $COMPOSER > "/opt/${FOLDER}/composer.json"

composer install -d /opt/${FOLDER}

for bin in /opt/${FOLDER}/vendor/bin/*
do
    file=${bin##*/}
    if [ ! -f "/usr/local/bin/${file}" ]; then
        ln -s "${bin}" /usr/local/bin/
    fi
done
