#!/bin/bash

BIN_PATH="/usr/local/bin"
EXEC="${BIN_PATH}/composer-2"

if [[ -x "${BIN_PATH}/composer-${COMPOSER_VERSION}" ]]; then
    EXEC="${BIN_PATH}/composer-${COMPOSER_VERSION}"
fi

$EXEC $@
