FROM php:7.2-cli-buster
LABEL Description="CI-CD"

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
    apt-get update && \
    apt-get upgrade -y
RUN apt-get install --no-install-recommends -y git nano tree curl jq unzip zip
RUN apt-get install --no-install-recommends -y libbz2-dev libenchant-dev libfreetype6-dev libjpeg62-turbo-dev libpng-dev libc-client-dev libkrb5-dev libicu-dev libpq-dev libpq-dev libpspell-dev libsnmp-dev libxml2-dev libxml2-dev libxslt-dev zlib1g-dev libzip-dev libmagickwand-dev
RUN docker-php-ext-configure bcmath && \
    docker-php-ext-install bcmath && \
    docker-php-ext-configure bz2 && \
    docker-php-ext-install bz2 && \
    docker-php-ext-configure calendar && \
    docker-php-ext-install calendar && \
    docker-php-ext-configure dba && \
    docker-php-ext-install dba && \
    docker-php-ext-configure enchant && \
    docker-php-ext-install enchant && \
    docker-php-ext-configure exif && \
    docker-php-ext-install exif && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install -j$(nproc) gd && \
    docker-php-ext-configure gettext && \
    docker-php-ext-install gettext && \
    docker-php-ext-configure imap --with-kerberos --with-imap-ssl && \
    docker-php-ext-install imap && \
    docker-php-ext-configure intl && \
    docker-php-ext-install intl && \
    docker-php-ext-configure mysqli && \
    docker-php-ext-install mysqli && \
    docker-php-ext-configure opcache && \
    docker-php-ext-install opcache && \
    docker-php-ext-configure pcntl && \
    docker-php-ext-install pcntl && \
    docker-php-ext-configure pdo_mysql && \
    docker-php-ext-install pdo_mysql && \
    docker-php-ext-configure pdo_pgsql && \
    docker-php-ext-install pdo_pgsql && \
    docker-php-ext-configure pgsql && \
    docker-php-ext-install pgsql && \
    docker-php-ext-configure pspell && \
    docker-php-ext-install pspell && \
    docker-php-ext-configure shmop && \
    docker-php-ext-install shmop && \
    docker-php-ext-configure snmp && \
    docker-php-ext-install snmp && \
    docker-php-ext-configure soap && \
    docker-php-ext-install soap && \
    docker-php-ext-configure sockets && \
    docker-php-ext-install sockets && \
    docker-php-ext-configure sysvmsg && \
    docker-php-ext-install sysvmsg && \
    docker-php-ext-configure sysvsem && \
    docker-php-ext-install sysvsem && \
    docker-php-ext-configure sysvshm && \
    docker-php-ext-install sysvshm && \
    docker-php-ext-configure xmlrpc && \
    docker-php-ext-install xmlrpc && \
    docker-php-ext-configure xsl && \
    docker-php-ext-install xsl && \
    docker-php-ext-configure zip && \
    docker-php-ext-install zip && \
    pecl install imagick && \
    docker-php-ext-enable imagick && \
    pecl install redis && \
    docker-php-ext-enable redis && \
    pecl install xdebug && \
    docker-php-ext-enable xdebug
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
RUN curl -sL https://deb.nodesource.com/setup_10.x | /bin/bash - && \
    apt-get install --no-install-recommends -y nodejs && \
    npm install -g n && \
    npm install -g npm@6.14.8 && \
    n 14.15.1 && \
    npm install -g gulp yarn
RUN apt-get purge -y --auto-remove && \
    rm -rf /var/lib/apt/lists/*

COPY _common/composer-require.sh /usr/local/bin/composer-require
COPY _common/php.ini /usr/local/etc/php/
COPY _common/phplint /usr/local/bin/phplint
COPY _common/phpsfdump /usr/local/bin/phpsfdump
RUN echo "xdebug.max_nesting_level = 10000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
COPY _common/composer.sh /usr/local/bin/composer
RUN curl -o /usr/local/bin/composer-1 https://getcomposer.org/composer-1.phar && \
    curl -o /usr/local/bin/composer-2 https://getcomposer.org/composer-2.phar && \
    chmod +x /usr/local/bin/composer*
RUN composer-require phpmd/phpmd "*" && \
    composer-require povils/phpmnd "*" && \
    composer-require maglnet/composer-require-checker "*" && \
    composer-require squizlabs/php_codesniffer "*" && \
    composer-require sebastian/phpcpd "*" && \
    composer-require sebastian/phpdcd "*" && \
    composer-require block8/php-docblock-checker "*" && \
    composer-require phpstan/phpstan "*" && \
    composer-require wapmorgan/php-deprecation-detector "*" && \
    composer-require phploc/phploc "*" && \
    composer-require phpmetrics/phpmetrics "*" && \
    composer-require phpunit/phpunit "^8" && \
    composer-require drupal/coder "*" && \
    composer-require mglaman/drupal-check "*"

WORKDIR /var/www/html
